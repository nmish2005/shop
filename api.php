<?php

namespace api\core;

require_once './vendor/autoload.php';

use MysqliDb;

$db = new MysqliDb ('31.41.219.226', 'misha_shopuser', 'rerepkbr', 'misha_shopdb');


$_POST = json_decode(file_get_contents('php://input'), true);
$action = $_POST["action"];

header("Access-Control-Allow-Origin: *");

switch ($action) {
    case 'load':
        $result = $db->get('products');
        echo json_encode($result);
        break;

    default:
        echo "Invalid action: ${$action}";
        http_response_code(405);
}